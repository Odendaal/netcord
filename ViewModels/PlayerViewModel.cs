﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NETCord.Models;

namespace NETCord.ViewModels
{
    public class PlayerViewModel : Player
    {
        public List<Player> PlayerList { get; set; }
    }
}