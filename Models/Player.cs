﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NETCord.Models
{
    public class Player
    {
        public string Name { get; set; }
        public string Rating { get; set; }
        public char Gender { get; set; }
        public string Location { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Team { get; set; }
        public string League { get; set; }
        public string Flight { get; set; }
        public string YearEndRatingDate { get; set; }
    }
}