﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HtmlAgilityPack;
using NETCord.DAL.USTA.Player;
using NETCord.Models;
using NETCord.ViewModels;

namespace NETCord.Controllers
{
    public class HomeController : Controller
    {

        #region Dependencies
        static readonly IPlayerRepository PlayerRepo = new PlayerRepository();
        #endregion

        public ActionResult Index(string playerName)
        {
            return View();
        }

        public ActionResult PlayerSearch(Player model)
        {
            var viewModel = new PlayerViewModel();

            if (ModelState.IsValid)
            {
                viewModel.PlayerList = PlayerRepo.GetPlayer(model.Name) as List<Player>;
            }

            return View("PlayerSearch", viewModel);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}