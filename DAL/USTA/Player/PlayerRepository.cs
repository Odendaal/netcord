﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using HtmlAgilityPack;
using log4net;

namespace NETCord.DAL.USTA.Player
{
    public class PlayerRepository : IPlayerRepository
    {
        private readonly ILog _logger = LogManager.GetLogger(typeof(PlayerRepository));
        private readonly String _url = WebConfigurationManager.AppSettings["USTAUrl"];

        public IEnumerable<Models.Player> GetPlayer(string playerName)
        {
            var fullName = playerName.Split(' ');
            var queryString = $"/reports/NTRP/SearchResults.aspx?Search=Name&SearchType=W&LastName={fullName[1]}&FirstName={fullName[0]}";

            try
            {
                var web = new HtmlWeb();
                var htmlDoc = web.Load(_url + queryString);
                
                // Gather player data from HTML Grid
                var nodes = htmlDoc.QuerySelectorAll("#DataGrid1 table tr td");
                var playerList = new List<Models.Player>();

                // Manipulate HTMLNodes to create our Player Models
                for (var i = 7; i < nodes.Count; i++)
                {
                    var player = new Models.Player
                    {
                        Name = nodes[i].InnerText,
                        Gender = Convert.ToChar(nodes[i + 1].InnerText),
                        City = nodes[i + 2].InnerText,
                        State = nodes[i + 3].InnerText,
                        Rating = nodes[i + 4].InnerText + " " + nodes[i + 6].InnerText,
                        YearEndRatingDate = nodes[i + 5].InnerText
                    };

                    // If player rating is empty, record doesn't exist so don't add it to the list
                    if (!player.Rating.Contains("0.0"))
                        playerList.Add(player);

                    // Increment i accordingly
                    i = i + 6;
                }

                return playerList;
            }
            catch (Exception ex)
            {
                _logger.Error($"{_url}{queryString} : Problem encountered retrieving player data.", ex);
            }

            return new List<Models.Player>();
        }

        public IEnumerable<Models.Player> GetPlayerDetails(string playerName)
        {
            var queryString = $"/Main/StatsAndStandings.aspx?t=R-17&search={playerName}";

            try
            {
                var web = new HtmlWeb();
                var htmlDoc = web.Load(_url + queryString);

                // Gather player data from HTML Grid
                var nodes = htmlDoc.QuerySelectorAll(".CommonTable.Segmented tbody tr .top");
                var playerList = new List<Models.Player>();

                // Manipulate HTMLNodes to create our Player Models
                for (var i = 0; i < nodes.Count; i++)
                {
                    var player = new Models.Player();
                    var teamDetails = nodes[i + 2].InnerText.Split(new[] { "&nbsp;&nbsp;-&nbsp;" }, StringSplitOptions.None);

                    player.Name = nodes[i].InnerText;
                    player.Location = nodes[i + 1].InnerText;
                    player.Team = teamDetails[0];
                    player.League = teamDetails[1];
                    player.Flight = teamDetails[2];

                    playerList.Add(player);

                    // Increment i accordingly
                    i = i + 2;
                }

                return playerList;
            }
            catch (Exception ex)
            {
                _logger.Error($"{_url}{queryString} : Problem encountered retrieving player data.", ex);
            }

            return new List<Models.Player>();
        }
    }
}