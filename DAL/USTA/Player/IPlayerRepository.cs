﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NETCord.DAL.USTA.Player
{
    public interface IPlayerRepository
    {
        IEnumerable<Models.Player> GetPlayer(string playerName);

        IEnumerable<Models.Player> GetPlayerDetails(string playerName);
    }
}